const Shopify = require('shopify-api-node');

const shopify = new Shopify({
    shopName: 'your-shop-name',
    apiKey: 'your-api-key',
    password: 'your-app-password'
});