const { Router } = require('express');

const shopify = require('../controllers/shopify');
const orders = require('../controllers/orders');

const router = new Router();

router.get('/install', shopify.install);
router.get('/install/callback', shopify.installCallback);

router.get('/orders', orders.getOrders);


module.exports = router;