require('dotenv').config();
const PORT = process.env.APP_PORT || 7000;

const express = require('express');
const app = express();

app.use('/shopify', require('./routes/shopify'));

app.use((_, res) => res.send('Route not found!'));

app.listen(PORT, () => console.info(`Shopify App listening on port ${PORT}!`));