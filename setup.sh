#!/bin/bash
npm install -g ngrok --silent
npm install -g pm2 --silent

echo "SHOPIFY_API_KEY:"
read SHOPIFY_API_KEY

echo "SHOPIFY_API_SECRET:"
read SHOPIFY_API_SECRET

####### Frontend Installation
echo "Starting Frontend Installation."
rm ./frontend/.env
cat ./frontend/.env.example | sed s/'$SHOPIFY_API_KEY'/$SHOPIFY_API_KEY/  >> ./frontend/.env
cd ./frontend && npm install --silent && cd ..
echo "Frontend Installation Done."


echo "Starting Backend Installation."
####### Backend Installation
rm ./backend/.env
cat ./backend/.env.example | sed s/'$SHOPIFY_API_KEY'/$SHOPIFY_API_KEY/ | sed s/'$SHOPIFY_API_SECRET'/$SHOPIFY_API_SECRET/ >> ./backend/.env
cd ./backend && npm install --silent && cd ..

echo "Backend Installation Done."


pm2 start ecosystem.config.js

echo "Do not close this window..."

ngrok start --all --config ./ngrok.yml