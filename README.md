Installation steps

1. Open terminal and run `bash ./scripts/start_public_server.sh`. (Note: Do not close terminal keep it open)
2. Go to your shopify partner account (https://partners.shopify.com/) and log in.
3. Go to Apps section and create the app.
4. In `App url` section copy/past uri from first step (htttps://0000000.ngrok.io)
5. After App Create go to `Extensions` section Enable Embedded apps `if not enabled`
6. Open `App Actions` dropdown and select Install development store.
7. Select one of your existing shopify websites in which you want to install the app.
8. 