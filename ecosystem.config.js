module.exports = {
    "apps": [{
        "name": "wellygen-backend",
        "cwd": "./backend",
        "script": "index.js",
        "watch": true
    }, {
        "name": "wellygen-frontend",
        "cwd": "./frontend",
        "script": "npm",
        "args": "start"
    }]
}