import React, { useEffect, useState } from 'react';
import { Context } from '@shopify/app-bridge-react';
import { Card, DataTable, Page } from '@shopify/polaris';

export default function Orders() {
    const rows = [
        ['Emerald Silk Gown', '$875.00', 124689, 140, '$122,500.00'],
        ['Mauve Cashmere Scarf', '$230.00', 124533, 83, '$19,090.00'],
        [
            'Navy Merino Wool Blazer with khaki chinos and yellow belt',
            '$445.00',
            124518,
            32,
            '$14,240.00',
        ],
    ];

    const [orders, setOrders] = useState([]);

    useEffect(function() {
        fetch('https://f8bc4656.ngrok.io/shopify/orders', function(data) {
            console.log("Incomming data is", data);

            setOrders(data);
        });

        return;
    });

    return (
        <Context.Consumer>
            {app => {
                console.log("loading......");
                // Do something with App Bridge `app` instance...
                if (app) {
                    app.getState().then(state => console.log("state", state));
                }

                return (
                    <Page title="Sales by product">
                        <Card>
                            <DataTable
                                columnContentTypes={[
                                    'text',
                                    'numeric',
                                    'numeric',
                                    'numeric',
                                    'numeric',
                                ]}
                                headings={[
                                    'Product',
                                    'Price',
                                    'SKU Number',
                                    'Net quantity',
                                    'Net sales',
                                ]}
                                rows={rows}
                                totals={['', '', '', 255, '$155,830.00']}
                            />
                        </Card>
                    </Page>
                );
            }}
        </Context.Consumer>
    );
}
