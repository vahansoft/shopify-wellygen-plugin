import React, { useState, useCallback } from 'react';
import ReactDOM from 'react-dom';
import { Provider, TitleBar, Loading, Modal } from '@shopify/app-bridge-react';
import enTranslations from '@shopify/polaris/locales/en.json';
import { AppProvider, Card, Button, Link, AccountConnection } from '@shopify/polaris';
import { FileUpload } from './components/file_upload';
import Orders from './components/orders';

function MyApp(props) {
    console.log("process.env.EMBEDED_SHOPIFY_API_KEY", process.env.REACT_APP_EMBEDED_SHOPIFY_API_KEY);
    const config = {
        apiKey: process.env.REACT_APP_EMBEDED_SHOPIFY_API_KEY, 
        shopOrigin: "testbarstore.myshopify.com"
    };

    return (
        <AppProvider i18n={enTranslations}>
            <Provider config={config}>
                <TitleBar title="My page title" />
                <Card>
                    <Orders />
                </Card>
            </Provider>
        </AppProvider>
    );
}


const root = document.getElementById('root');
ReactDOM.render(<MyApp />, root);